﻿using UnityEngine;
using System.Collections;

public class AccelerationToLightSaberOrientation : MonoBehaviour {

    public Quaternion quaternionWanted;
    public float degreeSpeed=200f;
    public float degreeMinimumToMove=2f;


    
	// Update is called once per frame
	void Update () {

        Vector3 verticalEuleurRot = Vector3.zero;
        Vector3 horizontalEuleurRot = Vector3.zero;
        Vector3 acceleration = Input.acceleration;

        //newEulerRot.y = -acceleration.z * 90f;

        if (acceleration.y < 0f) verticalEuleurRot.x = -90f * (1f - Mathf.Abs(Mathf.Clamp(acceleration.x, -1f, 0)));
        else if (acceleration.y >= 0f) verticalEuleurRot.x = 90f * (1f - Mathf.Abs(Mathf.Clamp(acceleration.x, -1f, 0)));
        acceleration.x += 90f;

        horizontalEuleurRot.y = - acceleration.z * 90f;
        quaternionWanted = Quaternion.Euler(verticalEuleurRot) * Quaternion.Euler(horizontalEuleurRot);

        transform.localRotation = quaternionWanted;

	}
}
