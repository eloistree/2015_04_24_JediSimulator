﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DebugText_UDP : MonoBehaviour {

    public Text tzSender;
    public Text tzReceiver;

    public UDP_Sender sender;
    public UDP_Receiver receiver;
	// Use this for initialization

    void Update() {
        tzSender.text = sender.lastSentPackage;
        tzReceiver.text = receiver.lastReceivedUDPPackage;
    
    }

    private void DisplaySenderInfo(UDP_Sender sender, string message)
    {
    }

    private void DisplayReceivedPackage(UDP_Receiver from, string message, string adresse, int port)
    {
    }
}
