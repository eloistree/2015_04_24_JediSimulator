﻿using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDP_Receiver : MonoBehaviour
{

    Thread receiveThread;
    UdpClient client;

    public int listenedPort = 8051;
    public bool withAutoStart = true;

    public string lastReceivedUDPPackage = "";
    public string allReceivedUDPPackages = ""; // clean up this from time to time!
    private int maxLenghtOfAllPackagesRecorded = 10000;

    public delegate void OnStartListening(UDP_Receiver who, int port);
    public delegate void OnReceivedUDPPackage(UDP_Receiver from, string message, string adresse, int port);
    public delegate void OnPackagesClear(UDP_Receiver from, string allMessage);
    public delegate void OnStopListening(UDP_Receiver who);

    public OnStartListening onStartListening;
    public OnReceivedUDPPackage onPackageReceived;
    public OnPackagesClear onPackagesClear;
    public OnStopListening onStopListening;

    public void Start()
    {
        if (withAutoStart)
            StartListening();
    }

    public void SetPortListened(int port) {
        if (port > 0)
            listenedPort = port;
    
    }
    
    
    public void StartListening(int port)
    {
        SetPortListened(port);
        StartListening();
    }

    public void StartListening()
    {
        if (IsListening()) return;

        receiveThread = new Thread(
            new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();

        if (onStartListening != null)
            onStartListening(this, listenedPort);
    }

    private bool IsListening()
    {
        return receiveThread != null && receiveThread.IsAlive;
    }

    private void ReceiveData()
    {

        client = new UdpClient(listenedPort);
        while (true)
        {

            try
            {
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = client.Receive(ref anyIP);
                string text = Encoding.UTF8.GetString(data);
                lastReceivedUDPPackage = text;
                allReceivedUDPPackages = allReceivedUDPPackages + text;

                if (onPackageReceived != null)
                    onPackageReceived(this, lastReceivedUDPPackage, anyIP.Address.ToString() ,anyIP.Port);
                if (allReceivedUDPPackages.Length > maxLenghtOfAllPackagesRecorded)
                    ClearPackagesRecorded();

            }
            catch (Exception err)
            {
                print(err.ToString());
            }
        }
    }

    public void ClearPackagesRecorded()
    {
        if (onPackagesClear != null)
            onPackagesClear(this, allReceivedUDPPackages);
        allReceivedUDPPackages = "";
    }

    public string GetLastPackage()
    {
        return lastReceivedUDPPackage;
    }

    void OnDestroy()
    {
        StopListening();
    }
    void OnApplicationQuit()
    {
        StopListening();
    }

    public void StopListening()
    {
        if (!IsListening()) return;
        if (receiveThread != null) { 
             receiveThread.Abort();
             receiveThread = null;
        }

        if (client != null)
        {
            client.Close();
            client = null;
        }
        if (onStopListening != null)
            onStopListening(this);
    }


    internal int GetListenedPort()
    {
        return listenedPort;
    }

    internal string GetLocalIp()
    {
        return Network.player.ipAddress;
    }
    internal string GetExternalIp()
    {
        return Network.player.externalIP;
    }
}
