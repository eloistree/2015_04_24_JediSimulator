﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VirtualControllerAccessor : MonoBehaviour {

    public static VirtualControllerAccessor Instance;
    public static List<VirtualControllerAccessor> InstancesInScene = new List<VirtualControllerAccessor>();
    public static Dictionary<string, VirtualControllerAccessor> PlayerInScene = new Dictionary<string, VirtualControllerAccessor>();
    public static Dictionary<int, VirtualControllerAccessor> PlayerIndexInScene = new Dictionary<int, VirtualControllerAccessor>();

    public VirtualController Controller = new VirtualController();
    public string description;
    public bool withDebug;

    public bool mainController;
    public int playerIndex = 0;
    public string playerIdKey = "Default";
    public void SetController(VirtualController controller) { this.Controller = controller; }

    void Start(){
        if( Instance==null || mainController)
            Instance = this;
        InstancesInScene.Add(this);
        if (!PlayerInScene.ContainsKey(playerIdKey))
        {
                PlayerInScene[playerIdKey] = this;
        }
        else Debug.LogWarning("Player already recorded at the key id:" + playerIdKey);


        if (!PlayerIndexInScene.ContainsKey(playerIndex))
        {
                PlayerIndexInScene[playerIndex] = this;
        }
        else Debug.LogWarning("Player already recorded at index:" + playerIndex);
            
    }

    public static VirtualControllerAccessor GetPlayerAccesor(string idKey) { return PlayerInScene[idKey]; }
    public static VirtualControllerAccessor GetPlayerAccesor(int indexIdKey) { return GetPlayerAccesor(indexIdKey); }
    public static VirtualController GetPlayerController(string idKey) { return GetPlayerAccesor(idKey).Controller; }
    public static VirtualController GetPlayerController(int indexIdKey) { return GetPlayerAccesor(indexIdKey).Controller; }
    void OnDestroy()
    {
        InstancesInScene.Remove(this);
        PlayerInScene.Remove(playerIdKey);
        PlayerIndexInScene.Remove(playerIndex);
    }


    void Update()
    {
        if (withDebug && Controller != null  )
        {
            description = Controller.GetActiveDataDescription();
                Debug.Log(description);
        }
    }
}
