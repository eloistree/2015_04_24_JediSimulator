﻿using UnityEngine;
using System.Collections;

public class LocalRotationToVirtualController : MonoBehaviour {

    public VirtualControllerAccessor accessor;
    public Transform headRef;
    public int index;
    
    void Update () {
        if (accessor && headRef)
            accessor.Controller.SetOrientationValue(index, headRef.localRotation);
	
	}
}
