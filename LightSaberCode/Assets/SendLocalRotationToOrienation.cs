﻿using UnityEngine;
using System.Collections;

public class SendLocalRotationToOrienation : MonoBehaviour {

    public VirtualControllerAccessor virtualController;
    public Transform target;
    public int orientationIndex;
	void Update () {
        virtualController.Controller.SetOrientationValue(orientationIndex, target.localRotation);
	}
}
