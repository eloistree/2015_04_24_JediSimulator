﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour {


    public static List<Enemy> EnemyInScene = new List<Enemy>();


    void Start() {
        EnemyInScene.Add(this);
    }
    void Destroy()
    {
        EnemyInScene.Remove(this);
    

    }

    public static List<Enemy> GetEnemyInCameraView(float radiusHorizontal = 0.25f, float radiusVertical = 0.25f)
    {
        
        float minHorizontal = 0.5f-Mathf.Abs(radiusHorizontal);
        float maxHorizontal = 0.5f+Mathf.Abs(radiusHorizontal);
        float minVertical = 0.5f-Mathf.Abs(radiusVertical);
        float maxVertical = 0.5f+Mathf.Abs(radiusVertical);

        List<Enemy> enemyInView = new List<Enemy>();
        foreach(Enemy enemy in EnemyInScene)
        foreach (Camera cam in Camera.allCameras)
        { 
            Vector3 viewPort = cam.WorldToViewportPoint(enemy.transform.position);
            if (viewPort.x > minHorizontal && viewPort.x < maxHorizontal && viewPort.y > minVertical && viewPort.y < maxVertical) 
            {
                enemyInView.Add(enemy);
            }
            
        }
        return enemyInView;
    
    }
    public static Enemy GetRandomEnemyInView(float radiusHorizontal = 0.25f, float radiusVertical = 0.25f)
    {
        List<Enemy> enemyInView = GetEnemyInCameraView(radiusHorizontal, radiusVertical);
        if (enemyInView.Count <= 0) return null;
        return enemyInView[Random.Range(0, enemyInView.Count)];
    
    }

    public static Enemy GetEnemyTheCloserInCameraView(float radiusHorizontal = 0.25f, float radiusVertical = 0.25f)
    {

        float minHorizontal = 0.5f - Mathf.Abs(radiusHorizontal);
        float maxHorizontal = 0.5f + Mathf.Abs(radiusHorizontal);
        float minVertical = 0.5f - Mathf.Abs(radiusVertical);
        float maxVertical = 0.5f + Mathf.Abs(radiusVertical);

        float closer = float.MaxValue;
        Enemy closerEnemy=null;
        foreach (Enemy enemy in EnemyInScene) {
            if (!enemy.isActiveAndEnabled) continue;
            foreach (Camera cam in Camera.allCameras)
            {

                Vector3 viewPort = cam.WorldToViewportPoint(enemy.transform.position);
                if (viewPort.x > minHorizontal && viewPort.x < maxHorizontal && viewPort.y > minVertical && viewPort.y < maxVertical)
                {
                    viewPort.x -= 0.5f;
                    viewPort.y -= 0.5f;
                    if (viewPort.magnitude < closer)
                    {

                        closer = viewPort.magnitude;
                        closerEnemy = enemy;
                    }
                }

            }
        }
        return closerEnemy;

    }
    



}
