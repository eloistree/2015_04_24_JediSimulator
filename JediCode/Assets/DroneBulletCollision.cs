﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HasLife))]
public class DroneBulletCollision : MonoBehaviour {

    private HasLife elementLife;
    public PoolGenerator explosionPool;
    void Start()
    {
        elementLife = GetComponent<HasLife>() as HasLife;
    }


    public void OnCollisionStay(Collision col)
    {

        CollisionDetected(col.gameObject);
    }

    

    public void OnTriggerStay(Collider col) {
        CollisionDetected(col.attachedRigidbody.gameObject);
    
    }
    private void CollisionDetected(GameObject colGamo)
    {
        //if (!(colGamo.layer == LayerMask.NameToLayer("Jedi")))
        //    return;
        Debug.Log("Coll Hit", colGamo);
        I_LaserBullet bullet = colGamo.GetComponent<I_LaserBullet>() as I_LaserBullet;
        if (bullet == null)
            return;

        Debug.Log("Drone Hit", colGamo);
        if (explosionPool != null && elementLife.Life <= 1)
        {
            Sound.Play("ExplosionDrone",1f,80);

            GameObject gamo = explosionPool.GetNextAvailable();
            gamo.transform.position = transform.position;
            if (gamo)
                gamo.SetActive(true);


        }
        elementLife.Life -= 1;
        bullet.DestroyBullet();

      
            //Debug.Break();
        colGamo.SetActive(false);
    }
}
