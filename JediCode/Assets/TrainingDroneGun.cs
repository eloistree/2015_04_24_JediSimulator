﻿using UnityEngine;
using System.Collections;

public interface I_LaserBullet
{
    void Fire(Vector3 from, Vector3 opposedDirection, float speed);
    void Ricochet(Vector3 direction, float dispertionPourcent);
    void DestroyBullet();

    void SetDirection(Vector3 direction);

    float GetSpeed();
    void SetSpeed(float newSpeed);
}

public class TrainingDroneGun : MonoBehaviour {


    public TrainingZone training;
    public PoolGenerator bulletPool = new PoolGenerator();
    public Transform [] fireFrom;
    public Transform target;

    public float bulletSpeed=2f;
    public float vagueness = 0.3f;

    void Start()
    {
        training.onFire += Fire;

    }

    private void Fire()
    {
        Fire(GetOrigine(), GetDestination(), bulletSpeed);
    }

    private void Fire(Vector3 from, Vector3 destination, float bulletSpeed)
    {
        if (from == Vector3.zero) return;
        if (destination == Vector3.zero) return;
        GameObject bullet = bulletPool.GetNextAvailable();
        if (bullet == null) return;
        I_LaserBullet laserBullet = bullet.GetComponent<I_LaserBullet>() as I_LaserBullet;
        if (laserBullet == null) return;
        laserBullet.Fire(from,(destination - from).normalized, bulletSpeed);

        bullet.gameObject.layer = LayerMask.NameToLayer("Enemy");
        bullet.SetActive(true);
    }

    private Vector3 GetDestination()
    {
        if(target==null) return Vector3.zero;
        Vector3 targetPosition = target.position;
        targetPosition.x += Random.Range(-vagueness, vagueness);
        targetPosition.y += Random.Range(-vagueness, vagueness);
        targetPosition.z += Random.Range(-vagueness, vagueness);
        return targetPosition;
    }

    private Vector3 GetOrigine()
    {
        Vector3 from= Vector3.zero;
        float distance= float.MaxValue;

        for (int i = 0; i < fireFrom.Length; i++)
        {
            float dist = Vector3.Distance(fireFrom[i].position, target.position);
            if (dist < distance)
            {
                distance = dist;
                from = fireFrom[i].position;
            }
               
        }
        return from;
    }


}
