﻿using UnityEngine;
using System.Collections;

public class CollisionSound : MonoBehaviour {


    public void OnCollisionEnter(Collision col)
    {

        CollisionDetected(col.gameObject);
    }
    public void OnTriggerEnter(Collider col)
    {
        CollisionDetected(col.gameObject);
    }

    public void OnEnable() {
        Sound.Play("DroneFireLaser", 100f, 80);
    }


    public void CollisionDetected(GameObject gamoCol) {

        if (gamoCol.layer == LayerMask.NameToLayer("Wall"))
            Sound.Play("WallHit", 100f, 80);
        if (gamoCol.layer == LayerMask.NameToLayer("Jedi"))
            if (gamoCol.GetComponent<LaserBulletRedirection>() != null)
                Sound.Play("BlockLightSaber", 100f, 90);
        if (gamoCol.layer == LayerMask.NameToLayer("Enemy"))
            Sound.Play("DroneTouch", 100f, 100);
    }
}
