﻿using UnityEngine;
using System.Collections;
using System;

public class TrainingZone : MonoBehaviour {

   
    public Transform zoneCenter;
    public float zoneRadius=5f;
    public float minHeight=0.5f;
    public float maxHeight=3f;


    public float minSpeed=1f;
    public float maxSpeed=3f;

    public float moveTimeMin=1f;
    public float moveTimeMax=2.3f;


    public Vector3 nextPosition;
    public float speed;
    public float nextFire;

    public bool debugStopMoving;

    public delegate void OnFire();
      public   OnFire onFire;

      private float nextAttackTime;
 

    public void Update() {

        transform.position = Vector3.MoveTowards(transform.position, nextPosition, Time.deltaTime * speed);
        nextFire = Mathf.Clamp(nextFire - Time.deltaTime, 0f, float.MaxValue);

        if (nextAttackTime > 0f)
            nextAttackTime -= Time.deltaTime;
        if (nextAttackTime <= 0)
            RefreshAttackTime();

    }

    private void RefreshAttackTime()
    {
            Fire();
            ResetData();
            nextAttackTime = nextFire = UnityEngine.Random.Range(moveTimeMin, moveTimeMax);

    }


    private void Fire()
    {
        if (onFire != null)
            onFire();
    }

    private void ResetData()
    {
        if (debugStopMoving) { nextPosition = transform.position; 
        }
        else {
            nextPosition = zoneCenter.position;
            nextPosition.y += UnityEngine.Random.Range(minHeight, maxHeight);
            nextPosition.x += UnityEngine.Random.Range(-zoneRadius, zoneRadius);
            nextPosition.z += UnityEngine.Random.Range(-zoneRadius, zoneRadius);

        }
         
        speed = UnityEngine.Random.Range(minSpeed, maxSpeed);
    }
	
}
