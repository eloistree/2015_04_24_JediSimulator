﻿using UnityEngine;
using System.Collections;

public class JediHandIK : MonoBehaviour {

    public Animator jediAnimator;
    public Transform leftHand;
    public Transform rightHand;

    void OnAnimatorIK(int Layer)
    {
        jediAnimator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
        jediAnimator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
        jediAnimator.SetIKPosition(AvatarIKGoal.LeftHand, leftHand.position);
        jediAnimator.SetIKPosition(AvatarIKGoal.RightHand, rightHand.position);

        jediAnimator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
        jediAnimator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
        jediAnimator.SetIKRotation(AvatarIKGoal.LeftHand, leftHand.rotation);
        jediAnimator.SetIKRotation(AvatarIKGoal.RightHand, rightHand.rotation);
    }
}
