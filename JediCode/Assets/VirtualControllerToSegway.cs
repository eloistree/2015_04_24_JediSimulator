﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(I_PlayerAction))]
public class VirtualControllerToSegway : MonoBehaviour {


    public VirtualControllerAccessor externaInput;
    public MonoBehaviour playerControlScript;
    private I_PlayerAction playerCharacterControl;
    

    void Start()
    {
        playerCharacterControl = playerControlScript as I_PlayerAction;

        externaInput.Controller.GetButton(0).onValueChanged += Jump;
        externaInput.Controller.GetButton(1).onValueChanged += ShieldBlock;
        externaInput.Controller.GetButton(2).onValueChanged += SwordSlash;
        externaInput.Controller.GetButton(3).onValueChanged += FireArrow;
        externaInput.Controller.GetButton(4).onValueChanged += RespawnTheCharacter;

        externaInput.Controller.GetJoystick(0).onValueChanged += ChangePlayerDirection;

    }

    private void ChangePlayerDirection(int index, Vector3 oldValue, Vector3 newValue)
    {
        playerCharacterControl.MoveCharacter(newValue);
    }

    private void ShieldBlock(int index, bool oldValue, bool newValue)
    {
        if(oldValue!=newValue)
            playerCharacterControl.ShieldBlock(newValue);
    }

    private void SwordSlash(int index, bool oldValue, bool newValue)
    {
        if (!oldValue && newValue)
            playerCharacterControl.SwordSlash();

    }

    private void FireArrow(int index, bool oldValue, bool newValue)
    {
        if (oldValue && !newValue)
            playerCharacterControl.FireArrow();
        else if (! oldValue && newValue)
                playerCharacterControl.LoadArrow();
    }

    private void RespawnTheCharacter(int index, bool oldValue, bool newValue)
    {
        if (!oldValue && newValue)
            playerCharacterControl.RespawnTheCharacter();
    }

    private void Jump(int index, bool oldValue, bool newValue)
    {
        if (!oldValue && newValue)
            playerCharacterControl.Jump();
    }




}
