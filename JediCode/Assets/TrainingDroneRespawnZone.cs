﻿using UnityEngine;
using System.Collections;

public class TrainingDroneRespawnZone : MonoBehaviour {

    public HasLife elementLife;
    public Transform element;
    public Transform respawnPoint;
    public float respawnTimer = 25f;
    // Use this for initialization
    void Start()
    {
        elementLife.onNoLifeAnymore += UnityDeath;
    }

    void Destroy()
    {
        elementLife.onNoLifeAnymore -= UnityDeath;
    }

    private void UnityDeath(HasLife obj)
    {
        element.gameObject.SetActive(false);
        elementLife.Reset(true);
        StartCoroutine(RespawnIn(respawnTimer));
        Debug.Log("Is Death",this.gameObject);
    }

    IEnumerator RespawnIn(float time) {

        yield return new WaitForSeconds(time);
        Debug.Log("respawn", this.gameObject);
        element.transform.position = respawnPoint.position;
        element.gameObject.SetActive(true);
    }
}
