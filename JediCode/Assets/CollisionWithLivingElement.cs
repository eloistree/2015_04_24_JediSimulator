﻿using UnityEngine;
using System.Collections;

public class CollisionWithLivingElement : MonoBehaviour {

    void OnCollisionEnter(Collision col) {
        Debug.Log("Collision");

       HasLife life = col.gameObject.GetComponent<HasLife>() as HasLife;

       life.Life -= 1;
       // Debug.Break();
    
    }
}
