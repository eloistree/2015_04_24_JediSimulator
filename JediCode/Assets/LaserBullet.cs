﻿using UnityEngine;
using System.Collections;


public class LaserBullet : MonoBehaviour, I_LaserBullet
{
    public Vector3 direction;
    public Rigidbody rigBody;

    public int richochet = 0;
    public float speed = 1f;
    public float lifeTime = 10f;
    private float lifeTimeCountDown = 10f;

    void Start() {
        lifeTimeCountDown = lifeTime;
    }
    public void Update()
    {
        if (direction == Vector3.zero)
            return;
 //       transform.position += direction * (speed * Time.deltaTime);
        rigBody.velocity = direction * speed;

        lifeTimeCountDown -= Time.deltaTime;
        if (lifeTimeCountDown < 0) {
            DestroyBullet();
            lifeTimeCountDown = lifeTime;
        }
        }
    //void OnCollisionEnter(Collision col)
    //{
    //    Ricochet(-direction, 1f);
    //}
    public void Fire(Vector3 from, Vector3 direction, float speed)
    {
        Fire(from, direction, speed, true);
    
    }
    public void Fire(Vector3 from, Vector3 direction, float speed, bool reinit)
    {
        if (reinit) { 
            richochet = 0;
            lifeTimeCountDown = lifeTime;
        }

        this.transform.position = from;
        this.direction = direction;
        this.speed = speed;
        transform.rotation = Quaternion.LookRotation(direction);
    }

    public void Ricochet(Vector3 opposedDirection, float dispertionPourcent)
    {
        //if (richochet >= 2)
        //{
        //    //DestroyBullet();
        //    return;
        //}
        float range = 1f - dispertionPourcent;
        Vector3 direction = opposedDirection;
        //direction.x += Random.Range(-range, range);
        //direction.y += Random.Range(-range, range);
        //direction.z += Random.Range(-range, range);
        //direction.Normalize();
        richochet++;
        Fire(transform.position, direction, speed, false);
        Debug.DrawRay(transform.position, direction*50f, Color.red, 60f);
    }

    public void DestroyBullet()
    {
        gameObject.SetActive(false);
        richochet = 0;
    }




    public void SetDirection(Vector3 direction)
    {
        Fire(this.transform.position, direction, this.speed);
    }


    public float GetSpeed()
    {
        return speed;
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }
}