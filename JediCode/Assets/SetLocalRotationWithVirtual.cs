﻿using UnityEngine;
using System.Collections;

public class SetLocalRotationWithVirtual : MonoBehaviour {

    public VirtualControllerAccessor accessor;
    public int playerIndex;
    public int orientationIndex;
    public Quaternion lastRotation;
    public Quaternion rotationWanted;
    public Quaternion rotationApply;
    public float degreeBeforeLerp = 5;

    public float moveSpeed = 45f;
    public float lerpSpeed = 5f;


    public Vector3 initialLocalPosition;
    public float horizontalMove = 0.1f;
    public float frontalMove = 0.1f;
    public float handMoveSpeed = 0.5f;

    void Start() {
        initialLocalPosition = transform.localPosition;
    }
	void Update () {
       
        rotationWanted =VirtualControllerAccessor.GetPlayerController(orientationIndex).GetOrientationValue(orientationIndex);
        SetOrientation();
        SetPosition();
	}

    private void SetPosition()
    {
        Vector3 euleurOrientation = transform.localRotation.eulerAngles;
       // Debug.Log("WWWW: " + euleurOrientation);
        float xTranslation=0f;
        float yTranslation=0f;
        bool toCloseOfCenter =euleurOrientation.y<2f && euleurOrientation.y>-2f;

        if (!toCloseOfCenter)
        {
            if (euleurOrientation.y >= 270f)
            {
                euleurOrientation.y = Mathf.Clamp(euleurOrientation.y, 270f, 360f);
                xTranslation = (euleurOrientation.y - 360f) / 90f;
            }
            else if (euleurOrientation.y <= 90f)
            {

                euleurOrientation.y = Mathf.Clamp(euleurOrientation.y, 0f, 90f);
                xTranslation = (euleurOrientation.y) / 90f;
            }


        }

        if (euleurOrientation.x < 270f) euleurOrientation.x = 360f;
        euleurOrientation.x = Mathf.Clamp(euleurOrientation.x, 270f, 360f);
        yTranslation = (euleurOrientation.x - 270f) / 90f;


        xTranslation = Mathf.Clamp(xTranslation, -1f, 1f);
        Vector3 newLocalPosition = initialLocalPosition;
        newLocalPosition.x += xTranslation * horizontalMove;
        newLocalPosition.z += yTranslation * frontalMove;
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, newLocalPosition, Time.deltaTime * handMoveSpeed);


    }

    private void SetOrientation()
    {
        float angle = Quaternion.Angle(rotationWanted, lastRotation);
        if (angle < degreeBeforeLerp)
            rotationApply = Quaternion.RotateTowards(transform.localRotation, rotationWanted, Time.deltaTime * moveSpeed);//*(Mathf.Clamp(angle/degreeBeforeLerp,0f,1f)));
        else rotationApply = Quaternion.Lerp(transform.localRotation, rotationWanted, Time.deltaTime * lerpSpeed);
        transform.localRotation = lastRotation = rotationApply;
    }
}
