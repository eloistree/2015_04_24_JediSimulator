﻿using UnityEngine;
using System.Collections;

public class LaserBulletRedirection : MonoBehaviour {

    public Transform directionReference;
    public float speedMultiplicator = 2f;
    public LayerMask environmentCollisionLayer;
    public float viewRange = 0.25f;

    public void OnCollisionEnter(Collision col)
    {
        CollisionDetected(col.gameObject);
    }

    private void CollisionDetected(GameObject gamoCollided)
    {
        if (!(gamoCollided.layer == LayerMask.NameToLayer("Enemy")))
            return;
        I_LaserBullet bullet = gamoCollided.GetComponent<I_LaserBullet>() as I_LaserBullet;
        if (bullet == null)
            return;

        Vector3 direction = directionReference.forward;
        RaycastHit rayHit;

        Enemy enemyInView = Enemy.GetEnemyTheCloserInCameraView(viewRange, viewRange);
        if (enemyInView)
        {
            direction = (enemyInView.transform.position - gamoCollided.transform.position).normalized;
            // Debug.Log("Enemy in view " + direction);

        }
        else
            if (Physics.Raycast(directionReference.position, directionReference.forward, out rayHit, float.MaxValue, environmentCollisionLayer))
            {
                direction = (rayHit.point - gamoCollided.transform.position).normalized;
                //Debug.Log("dir " + direction);
            }
        Debug.DrawRay(gamoCollided.transform.position, direction * 50f, Color.green, 60f);
        bullet.Ricochet(direction, 1f);
        bullet.SetSpeed(bullet.GetSpeed() * speedMultiplicator);
        gamoCollided.layer = LayerMask.NameToLayer("Jedi");
    }

    public void OnTriggerEnter(Collider col) {
        CollisionDetected(col.gameObject);
    }

    

}
