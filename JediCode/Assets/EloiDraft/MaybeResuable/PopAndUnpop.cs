﻿using UnityEngine;
using System.Collections;

public class PopAndUnpop : MonoBehaviour {
    public Transform target;
    public Vector3 localSizeOff = Vector3.zero;
    public Vector3 localSizeOn = Vector3.one;
    public float scaleSpeed=1f;
    public bool onOff;


    public void Reset() {
        if (target == null)
            target = transform;
    }
    public void Update()
    {
        if (onOff && transform.localScale != localSizeOn)
            transform.localScale = Vector3.MoveTowards(transform.localScale, localSizeOn, Time.deltaTime * scaleSpeed);
        if (!onOff && transform.localScale != localSizeOff)
            transform.localScale = Vector3.MoveTowards(transform.localScale, localSizeOff, Time.deltaTime * scaleSpeed);
    }


    public void OnEnable()
    {
        transform.localScale = localSizeOff;
        Pop(true);
    }
    public void OnDisable()
    {
        transform.localScale = localSizeOff;
    }
    public void Pop(bool onOff) { this.onOff = onOff; }


	


}
