﻿using UnityEngine;
using System.Collections;

public class RotateQuickerWithFocus : MonoBehaviour, IScreenInteractionListener, IScreenInteractionCustom
{

    public QuickRotate rotateManager;
    public float timeToLoad =10f;
    public float normalSpeed = 10f;
    public float finalSpeed = 180f;
    private float wantedSpeed = 10f;
    public bool cursorNeeded;

    public void Update()
    {

        if (rotateManager.speed != wantedSpeed) 
        {
            float speed = Mathf.MoveTowards(rotateManager.speed, wantedSpeed, Time.deltaTime * wantedSpeed);
            rotateManager.speed = speed;
        }
    }


    public void OnEnable(){
        wantedSpeed = normalSpeed;
    }


    public void OnScreenInteractionSubmit(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionEnter(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionDown(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionOver(ScreenToIngameInteraction from, string device, float timeLeft, float initalTime)
    {

        float pourcent =1f-( Mathf.Clamp(timeLeft,0f,initalTime)/initalTime);
        wantedSpeed = finalSpeed + (finalSpeed - normalSpeed) * pourcent;
    }

    public void OnScreenInteractionStay(ScreenToIngameInteraction from, string device, float timeLeft, float initalTime)
    {
    }

    public void OnScreenInteractionUp(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionExit(ScreenToIngameInteraction from, string device)
    {
        wantedSpeed = normalSpeed;
    
    }


    public float GetTimeToBeSubmitOnOver()
    {
        return timeToLoad;
    }

    public float GetTimeToBeSubmitOnStay()
    {
        return timeToLoad;
    }


    public bool IsDisplayCursorNeeded()
    {
        return cursorNeeded;
    }
}
