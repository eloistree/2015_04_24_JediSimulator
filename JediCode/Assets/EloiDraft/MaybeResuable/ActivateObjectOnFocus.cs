﻿using UnityEngine;
using System.Collections;

public class ActivateObjectOnFocus : MonoBehaviour, IScreenInteractionListener, IScreenInteractionCustom
{

    public GameObject activateOnFocus;
    public float timeToBeLoad=0.5f;
    public bool neededCursor;

    public void ActivateOnFocus() 
    {
        if(activateOnFocus)
        activateOnFocus.SetActive(true);
    }

    public void OnScreenInteractionSubmit(ScreenToIngameInteraction from, string device)
    {
        ActivateOnFocus();
    }

    public void OnScreenInteractionEnter(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionDown(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionOver(ScreenToIngameInteraction from, string device, float timeLeft, float initalTime)
    {
    }

    public void OnScreenInteractionStay(ScreenToIngameInteraction from, string device, float timeLeft, float initalTime)
    {
    }

    public void OnScreenInteractionUp(ScreenToIngameInteraction from, string device)
    {
    }

    public void OnScreenInteractionExit(ScreenToIngameInteraction from, string device)
    {
    }



    public float GetTimeToBeSubmitOnStay()
    {
        return timeToBeLoad;
    }

    public float GetTimeToBeSubmitOnOver()
    {
        return timeToBeLoad;
    }


    public bool IsDisplayCursorNeeded()
    {
        return neededCursor && ! activateOnFocus.activeInHierarchy;
    }
}
