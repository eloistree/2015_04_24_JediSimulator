﻿using UnityEngine;
using System.Collections;

public class InterfacePlayerAnimator : MonoBehaviour {

    public Animator playerAnimator;
    public enum PlayerPosition{SitDown,StandUp, SitLeft,SitRight}
    public string standUp = "IsUp";
    public string sitLeft = "IsLeft";
    public string sitRight = "IsRight";


    public void SetPlayerPosition(PlayerPosition position) {


        playerAnimator.SetBool(sitLeft, false);
        playerAnimator.SetBool(sitRight, false);
        playerAnimator.SetBool(standUp, false);
        switch (position)
        {
            case PlayerPosition.StandUp:
                playerAnimator.SetBool(standUp, true);
                break;
            case PlayerPosition.SitLeft:
                playerAnimator.SetBool(sitLeft, true);
                break;
            case PlayerPosition.SitRight:
                playerAnimator.SetBool(sitRight, true);
                break;
            default:
                break;
        }
    
    }
    public void SetPlayerPositionValue(PlayerPosition position,bool value)
    {


    
        switch (position)
        {
           
            case PlayerPosition.StandUp:
                playerAnimator.SetBool(standUp, value);
                break;
            case PlayerPosition.SitLeft:
                playerAnimator.SetBool(sitLeft, value);
                break;
            case PlayerPosition.SitRight:
                playerAnimator.SetBool(sitRight, value);
                break;
            default:
                break;
        }

    }
	
}
