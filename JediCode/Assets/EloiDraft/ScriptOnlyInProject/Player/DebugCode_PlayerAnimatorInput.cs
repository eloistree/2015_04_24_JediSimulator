﻿using UnityEngine;
using System.Collections;

public class DebugCode_PlayerAnimatorInput : MonoBehaviour {

    public InterfacePlayerAnimator playerAnimatorInterface;
	
	void Update () {

        playerAnimatorInterface.SetPlayerPositionValue(InterfacePlayerAnimator.PlayerPosition.SitLeft, Input.GetKey(KeyCode.LeftArrow));
        playerAnimatorInterface.SetPlayerPositionValue(InterfacePlayerAnimator.PlayerPosition.SitRight, Input.GetKey(KeyCode.RightArrow));
        playerAnimatorInterface.SetPlayerPositionValue(InterfacePlayerAnimator.PlayerPosition.StandUp, Input.GetKey(KeyCode.UpArrow));
	
	}
}
