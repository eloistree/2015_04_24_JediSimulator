﻿using UnityEngine;
using System.Collections;

public class VirtualControllerToHeadRotation : MonoBehaviour {

    public VirtualControllerAccessor accessor;
    public HeadRotationLimit headRotation;
    public int headOrientationIndex = 0;
	void Start () {
        accessor.Controller.GetOrientation(0).onValueChanged += HeadRotate;

	}

    void OnDestroy() {
        accessor.Controller.GetOrientation(0).onValueChanged -= HeadRotate;
 

    }

    private void HeadRotate(int index, Quaternion oldValue, Quaternion newValue)
    {
        headRotation.SetHeadOrientation(newValue);
    }
	
}
