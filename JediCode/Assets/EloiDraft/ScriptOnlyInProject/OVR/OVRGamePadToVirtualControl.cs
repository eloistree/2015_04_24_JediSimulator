﻿using UnityEngine;
using System.Collections;

public class OVRGamePadToVirtualControl : MonoBehaviour {

    public VirtualControllerAccessor accessor;
    public OVRButtonIndex[] buttonListened= new OVRButtonIndex[]{
    
        new OVRButtonIndex(){button = OVRGamepadController.Button.A, virtualButtonIndex=0},
        new OVRButtonIndex(){button = OVRGamepadController.Button.B, virtualButtonIndex=1},
        new OVRButtonIndex(){button = OVRGamepadController.Button.X, virtualButtonIndex=2},
        new OVRButtonIndex(){button = OVRGamepadController.Button.Y, virtualButtonIndex=3},
        new OVRButtonIndex(){button = OVRGamepadController.Button.LeftShoulder, virtualButtonIndex=4},
        new OVRButtonIndex(){button = OVRGamepadController.Button.RightShoulder, virtualButtonIndex=5},
        new OVRButtonIndex(){button = OVRGamepadController.Button.Start, virtualButtonIndex=6},
        new OVRButtonIndex(){button = OVRGamepadController.Button.Back, virtualButtonIndex=7}
    };

    public OVRJoystickWithAxis[] joystickListened = new OVRJoystickWithAxis[] 
    {
        new OVRJoystickWithAxis(){axisHorizontal=OVRGamepadController.Axis.LeftXAxis, axisVertical=OVRGamepadController.Axis.LeftYAxis,virtualButtonIndex=0},
        new OVRJoystickWithAxis(){axisHorizontal=OVRGamepadController.Axis.RightXAxis, axisVertical=OVRGamepadController.Axis.RightYAxis,virtualButtonIndex=1}  
    };

    public OVRJoystickWithButton arrows = new OVRJoystickWithButton() { 
    up = OVRGamepadController.Button.Up,
    right = OVRGamepadController.Button.Right,
    down = OVRGamepadController.Button.Down,
    left = OVRGamepadController.Button.Left,
    virtualButtonIndex =2
    
    };

    public OVRAxis[] axiesListened = new OVRAxis[] 
    {
        new OVRAxis(){axis=OVRGamepadController.Axis.LeftTrigger, virtualButtonIndex =0},
        new OVRAxis(){axis=OVRGamepadController.Axis.RightTrigger, virtualButtonIndex =1}
    };

    void Update () {
        if (accessor != null)
        {
            foreach (OVRButtonIndex but in buttonListened)
                accessor.Controller.SetButtonValue(but.GetIndex(), but.GetButton());
            foreach (OVRJoystickWithAxis but in joystickListened)
                accessor.Controller.SetJoystickValue(but.GetIndex(), but.GetJoystick());
            foreach (OVRAxis but in axiesListened)
                accessor.Controller.SetTriggerValue(but.GetIndex(), but.GetJoystick());
            accessor.Controller.SetJoystickValue(arrows.GetIndex(), arrows.GetJoystick());
        }
	}

    [System.Serializable]
    public struct OVRButtonIndex
    {   
        public OVRGamepadController.Button button;
        public int virtualButtonIndex ;
        public int GetIndex() { return virtualButtonIndex; }
        public bool GetButton() {
            return OVRGamepadController.GPC_GetButton(button);
        }
    }

    public class OVRJoystick
    {
        public int virtualButtonIndex;

        public int GetIndex() { return virtualButtonIndex; }
     
    }

    [System.Serializable]
    public class OVRJoystickWithAxis : OVRJoystick
    {

        public OVRGamepadController.Axis axisHorizontal;
        public OVRGamepadController.Axis axisVertical;
        public Vector2 GetJoystick()
        {
            Vector2 joy = Vector2.zero;
            joy.x = OVRGamepadController.GPC_GetAxis(axisHorizontal);
            joy.y = OVRGamepadController.GPC_GetAxis(axisVertical);
            return joy;
        }
    }
    [System.Serializable]
    public class OVRAxis : OVRJoystick
    {

        public OVRGamepadController.Axis axis;

        public float GetJoystick()
        {
            return OVRGamepadController.GPC_GetAxis(axis);
        }
    }


    [System.Serializable]
    public class OVRJoystickWithButton : OVRJoystick
    {

        public OVRGamepadController.Button up = OVRGamepadController.Button.Up;
        public OVRGamepadController.Button right = OVRGamepadController.Button.Right;
        public OVRGamepadController.Button down = OVRGamepadController.Button.Down;
        public OVRGamepadController.Button left = OVRGamepadController.Button.Left;
        public Vector2 GetJoystick()
        {
            Vector2 joy = Vector2.zero;
            joy.x += OVRGamepadController.GPC_GetButton(up) ? 1f : 0f;
            joy.x -= OVRGamepadController.GPC_GetButton(down) ? 1f : 0f;
            joy.y += OVRGamepadController.GPC_GetButton(right) ? 1f : 0f;
            joy.y -= OVRGamepadController.GPC_GetButton(left) ? 1f : 0f;
            return joy;
        }
    }
}

