﻿using UnityEngine;
using System.Collections;

public class SegwayControl : MonoBehaviour, I_PlayerAction {

    #region Inspector Params
    public Transform startPoint;
    public Transform root;
    public Transform directionReference;
    public Transform lookPositionReference;
    public Rigidbody targetRigidBody;

    public ForceMode forceMode = ForceMode.Impulse;
    public float speedForce = 1f;
    public float jumpForce = 5f; 
    #endregion


    public Vector3 SegwayLastDirection;
    public float arrowLoadingSince;
    public bool IsLoadingArrow { get; private set; }
    public Vector2 playerDirection;


    #region Delegate Zone
    public delegate void StartLoadingArrow();
    public delegate void OnFireArrow(Vector3 destination, float arrowLoadingTime);

    public StartLoadingArrow onStartLoadingArrow;
    public OnFireArrow onFireArrow;

    public delegate void SlashWithSword();
    public SlashWithSword onSlashWithSword;

    public delegate void PlayerRepop(Vector3 where);
    public PlayerRepop onPlayerRepop;

    #endregion


    void Start()
    {

        if (directionReference == null)
            directionReference = Camera.main.transform;  
    }


	void Update () {

        Vector2 joystick = playerDirection;
        Vector3 worldPlayerDirection = RemoveHeightAxeAndNormalize(directionReference.right) * joystick.x;
        worldPlayerDirection += RemoveHeightAxeAndNormalize(directionReference.forward) * joystick.y;
        targetRigidBody.AddForce(worldPlayerDirection * ( Time.deltaTime * speedForce), forceMode);
        SegwayLastDirection = worldPlayerDirection.normalized ;
	}

    private Vector3 RemoveHeightAxeAndNormalize(Vector3 direction)
    {
        direction.y = 0;
        direction.Normalize();
        return direction;
    }




    private void Jump(int index, bool oldValue, bool newValue)
    {
        if (newValue)
            Jump();
    }
   

    private bool JumpConditionOk()
    {
        //TODO check cooldown and ground
        return true;
    }
    public void Jump()
    {
        if (JumpConditionOk())
        {
            targetRigidBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    public void FireArrow()
    {
        if (IsLoadingArrow)
        {
            if (onFireArrow != null)
            {
                onFireArrow(lookPositionReference.position, arrowLoadingSince);
            }
        }
      
        IsLoadingArrow = false;
    }

    public void SwordSlash()
    {
        if (onSlashWithSword != null)
        {
            onSlashWithSword();
        }
    }

    public void ShieldBlock(bool shieldOn)
    {
        Debug.Log("Blocking");
    }

    public void MoveCharacter(Vector2 moveDirectionJoystickValue)
    {
        playerDirection = moveDirectionJoystickValue;
    }

    public void RespawnTheCharacter()
    {
        if (startPoint == null) return;
        root.position = startPoint.position;
        if (onPlayerRepop != null)
            onPlayerRepop(startPoint.position);
    }


    public void LoadArrow()
    {  
        if ( ! IsLoadingArrow )
        {
            if (onStartLoadingArrow != null)
            {
                onStartLoadingArrow();
            }
        }
    }
}
