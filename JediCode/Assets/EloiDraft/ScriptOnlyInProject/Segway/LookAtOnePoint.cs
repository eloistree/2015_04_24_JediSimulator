﻿using UnityEngine;
using System.Collections;

public class LookAtOnePoint : MonoBehaviour {

    public SegwayControl segway;
    public Transform lookAtTransform;
    public Vector3 pointObservered;
    public Transform root;

    public Quaternion wantedOrientation;
    public float rotationSpeed = 80f;
    public bool loadingArrowPriority;

    public void Start()
    {
        segway.onStartLoadingArrow += AimTarget;
        segway.onFireArrow += StopAimTarget;

    }
    public void OnDestroy()
    {
        segway.onStartLoadingArrow -= AimTarget;
        segway.onFireArrow -= StopAimTarget;

    }

 
	void Update () {
        if (lookAtTransform != null)
            pointObservered = lookAtTransform.position;


        if ( !loadingArrowPriority && segway.SegwayLastDirection != Vector3.zero)
            wantedOrientation = LookAtDirection(segway.SegwayLastDirection);
        else
            wantedOrientation = LookAtPoint();
        root.rotation = Quaternion.RotateTowards(root.rotation, wantedOrientation, rotationSpeed);
	}

    private Quaternion LookAtDirection(Vector3 direction)
    {
        direction.y = 0;
       return  Quaternion.LookRotation(direction);
    }

    private Quaternion LookAtPoint()
    {
        Vector3 direction = (pointObservered-root.position).normalized;
        Quaternion orientation = Quaternion.LookRotation(direction);
        Vector3 eulerOrientation = orientation.eulerAngles;
        eulerOrientation.x = 0;
        return orientation = Quaternion.Euler(eulerOrientation);
    }




    private void StopAimTarget(Vector3 destination, float arrowLoadingTime)
    {
        loadingArrowPriority = false;
    }

    private void AimTarget()
    {
        loadingArrowPriority = true;
    }

}
