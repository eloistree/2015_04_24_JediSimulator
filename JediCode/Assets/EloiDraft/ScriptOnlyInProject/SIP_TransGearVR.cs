﻿using UnityEngine;
using System.Collections;

public class SIP_TransGearVR : SIP_Transform {

    public OVRGamepadController.Button[] buttonsToValidate = new OVRGamepadController.Button[]{OVRGamepadController.Button.A, OVRGamepadController.Button.Up};


    public override bool IsOneCursorsActive()
    {
        bool isPressingWithController=false;
        if(OVRGamepadController.GPC_IsAvailable())
            foreach (OVRGamepadController.Button but in buttonsToValidate)
                if (OVRGamepadController.GPC_GetButton(but)) {
                    isPressingWithController = true;
                    break;
                }

        return base.IsOneCursorsActive() || isPressingWithController ;
    }
   
    
}
