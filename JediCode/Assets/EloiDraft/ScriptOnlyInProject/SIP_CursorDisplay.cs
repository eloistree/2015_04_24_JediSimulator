﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SIP_CursorDisplay : MonoBehaviour {

    public ScreenToIngameInteraction screenCursor;

    public Transform cursorDirectionRef;

    public CursorState cursorState; 
    public GameObject aimObject;
    public GameObject focusObject;
    public Image loadingImage;
    public bool Visible;
    public bool aimActiveWhenFocus=false;

    public float maxDistance=6f;
    public LayerMask layersallow;

    public enum CursorState {None, Aiming, Focusing, Loading,FinishLoading}
    public float lastPourcentLoaded;

    void Start() {
        screenCursor.onMouseStay += ListenToStayEvens;
        screenCursor.onMouseHover += ListenToHoverEvens;
        screenCursor.onMouseExit += SwitchTarget;
    }

    private void SwitchTarget(ScreenToIngameInteraction screenInter)
    {
        lastPourcentLoaded = 0f;
    }

    void OnDestroy()
    {
        screenCursor.onMouseStay -= ListenToStayEvens;
        screenCursor.onMouseHover -= ListenToHoverEvens;
        screenCursor.onMouseExit -= SwitchTarget;

    }

    private void ListenToStayEvens(ScreenToIngameInteraction screenInter, float timeLeft, float initialTime)
    {
        SetPourcentLoad(timeLeft, initialTime);
    }
    private void ListenToHoverEvens(ScreenToIngameInteraction screenInter, float timeLeft, float initialTime)
    {
         SetPourcentLoad(timeLeft, initialTime);
    }

    public void SetPourcentLoad(float pourcent) {

        lastPourcentLoaded = pourcent;
        loadingImage.fillAmount = lastPourcentLoaded;
    }
    public void SetPourcentLoad(float timeLeft, float initialTime)
    {
        float pourcent = 1f;
        if (initialTime > 0f)
        {
           pourcent = 1f - (Mathf.Clamp(timeLeft, 0f, initialTime) / initialTime);
        }
        if (pourcent > lastPourcentLoaded)
            SetPourcentLoad(pourcent);
    }

    public void Update() {

        if (screenCursor == null) return;
        if (!Visible) { 
            SetCursorState(CursorState.None);
            return; 
        }

        if (screenCursor.IsOnFocusableObject)
        {
            if (lastPourcentLoaded <= 0f)
            {
                SetCursorState(CursorState.Focusing);
            }
            else if (lastPourcentLoaded < 1f) SetCursorState(CursorState.Loading);
            else SetCursorState(CursorState.FinishLoading);
        }
        else SetCursorState(CursorState.Aiming);

        RefreshPositionOfCursor();
    }

    private void RefreshPositionOfCursor()
    {
        if(cursorDirectionRef==null)return ;

        Vector3 origine = cursorDirectionRef.position;
        Vector3 direction = cursorDirectionRef.forward.normalized;

        RaycastHit[] hits = Physics.RaycastAll(origine, direction, maxDistance, layersallow);
        Vector3 pos = origine + direction * maxDistance;
        float distance= maxDistance;
        foreach (RaycastHit h in hits)
        {
            float d = Vector3.Distance(origine, h.point);
            if(d<distance){
                pos = h.point;
                distance=d;
            }
       }

        transform.position = pos; 
        
    }

    public void SetCursorState(CursorState newCursorState)
    {
        if (newCursorState == cursorState) return;
        bool cursorNeeded = true;
        if (screenCursor != null && screenCursor.SelectedObjectCustom != null)
            cursorNeeded = screenCursor.SelectedObjectCustom.IsDisplayCursorNeeded();
        
        if (newCursorState == CursorState.None)
        {
            SetPourcentLoad(0f);
            aimObject.SetActive(false);
            focusObject.SetActive(false);
            loadingImage.gameObject.SetActive(false);
        }
        else if ((newCursorState == CursorState.Loading || newCursorState == CursorState.Focusing) && cursorNeeded)
        {
            aimObject.SetActive(aimActiveWhenFocus);
            focusObject.SetActive(true);
            loadingImage.gameObject.SetActive(true);
        }
        else if (newCursorState == CursorState.Aiming || !cursorNeeded)
        {
            SetPourcentLoad(0f);
            aimObject.SetActive(true);
            focusObject.SetActive(false);
            loadingImage.gameObject.SetActive(false);
        }
       

            cursorState= newCursorState;
    }
}
