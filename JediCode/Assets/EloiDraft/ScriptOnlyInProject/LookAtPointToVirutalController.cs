﻿using UnityEngine;
using System.Collections;

public class LookAtPointToVirutalController : MonoBehaviour {


    public Transform cursorPoint;
    public VirtualControllerAccessor accessor;
    public int index;
    
    void Update () {
        if (accessor  && cursorPoint)
        {
            accessor.Controller.SetPositionValue(index, cursorPoint.position);
	
        }
        
	}
}
