﻿using UnityEngine;
using System.Collections;

public class SpineWhenLooked : MonoBehaviour {

    public bool turn;
    public float speed=90f;

    public void Update() {
        if(turn)
            transform.Rotate(Vector3.up, speed*Time.deltaTime);
    }



    public void OnOculusEnter()
    {
        turn = true;
        Debug.Log("Enter");
    }
    public void OnOculusStay()
    {
        Debug.Log("Stay");
    }
    public void OnOculusOver()
    {
        turn = true;
        Debug.Log("Over");
    }

    public void OnOculusUp()
    {
        Debug.Log("Up");
    }
    public void OnOculusDown()
    {
        Debug.Log("Down");
    }
    public void OnOculusExit()
    {
        turn = false;
        Debug.Log("Exit");
    }
    public void OnOculusSubmit()
    {
        Debug.Log("Submit");
    }
   

}
