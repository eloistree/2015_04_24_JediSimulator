﻿using UnityEngine;
using System.Collections;

/**
 Avatar = player model in the game
 Character = player hero that he control in game
 */

public interface I_Crossbow
{
    void FireArrow(Vector3 origine, Vector3 destination, float loadedTime);
}

public interface I_Sword
{
    void Slash(Vector2 direction);
}
public interface I_Cannon {

    void FireCannonball(Vector3 where, float loadedTime);
}

public interface I_PlayerAction
{
    void Jump();
    void LoadArrow();
    void FireArrow();
    void SwordSlash();
    void ShieldBlock(bool shieldOn);

    void MoveCharacter(Vector2 moveDirectionJoystickValue);
    void RespawnTheCharacter();
}


public enum PlayerAvatarPosition{ StandUp, LookingLeft, LookingRight, SitDown}
public enum PlayerAvatarAction{ HitPlayer, HitTable }//Could be add later: ThrowBall, MakePhone Ring ...
public interface I_PlayerAvatar
{
    void SetPlayerHeadOrientation( Quaternion localRotationOfhead );
    void SetPlayerBodyPosition( PlayerAvatarPosition bodyPosition );
    void DoAvatarAction( PlayerAvatarAction actionToDo );

}

public interface I_Menu
{
    void DisplayMenuInGame();
    void DisplayMenuSection(string specificPageName);
}

public interface I_RotatingTable
{
    void TurnBoardGameLeft();
    void TurnBoardGameRight();
    void FlipBoardGame();
}