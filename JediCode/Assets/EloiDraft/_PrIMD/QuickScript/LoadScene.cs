﻿using UnityEngine;
using System.Collections;
using System;

public class LoadScene : MonoBehaviour {

    public string sceneNameToLoad ="";

    public delegate void DoBeforeLoadNextScene(string currentScene, string nextScene);
    public static DoBeforeLoadNextScene WhatToDoBeforeLoadNextScene;
    public void LoadSelectedScene()
    {
        LoadNextScene(sceneNameToLoad);
    }
    public void LoadCurrentScene()
    {
        LoadNextScene(Application.loadedLevelName);
    }
    public void LoadSelectedScene(string sceneName)
    {
        LoadNextScene(sceneName);
    }
    public static void LoadNextScene(string nextScene) {
        try {
            if (WhatToDoBeforeLoadNextScene != null)
                WhatToDoBeforeLoadNextScene(Application.loadedLevelName, nextScene);
            if (string.IsNullOrEmpty(nextScene))
                Application.Quit();
            else
                Application.LoadLevel(nextScene);

        }
        catch(Exception e)
        {
            Debug.LogWarning("Impossible to load next scene:" + e);
        }
    }
}
