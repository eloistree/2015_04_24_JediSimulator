﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VirtualControllerDisplay : MonoBehaviour {


    public VirtualControllerAccessor controller;
    public string description;
    public Text displayInText;
    public bool withDebug;

    void Update() {
        if (controller != null)
        {
            description = controller.Controller.GetActiveDataDescription();
            if(displayInText)
                displayInText.text = description;
            if(withDebug)
                Debug.Log(description);
        }
    }

}
