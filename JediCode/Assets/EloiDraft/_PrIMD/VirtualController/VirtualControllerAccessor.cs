﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VirtualControllerAccessor : MonoBehaviour {

    public static VirtualControllerAccessor Instance;
    public static List<VirtualControllerAccessor> InstancesInScene = new List<VirtualControllerAccessor>();
    public static Dictionary<string, VirtualControllerAccessor> PlayerInScene = new Dictionary<string, VirtualControllerAccessor>();
    public static Dictionary<int, VirtualControllerAccessor> PlayerIndexInScene = new Dictionary<int, VirtualControllerAccessor>();

    public VirtualController Controller = new VirtualController();
    public string description;
    public bool withDebug;

    public bool mainController;
    public int playerIndex = 0;
    public string playerIdKey = "Default";
    public void SetController(VirtualController controller) { this.Controller = controller; }

    void Start(){
        if( Instance==null || mainController)
            Instance = this;
        InstancesInScene.Add(this);
        if (!PlayerInScene.ContainsKey(playerIdKey))
        {
                PlayerInScene[playerIdKey] = this;
        }
        else Debug.LogWarning("Player already recorded at the key id:" + playerIdKey);


        if (!PlayerIndexInScene.ContainsKey(playerIndex))
        {
                PlayerIndexInScene[playerIndex] = this;
        }
        else Debug.LogWarning("Player already recorded at index:" + playerIndex);
            
    }

    public static VirtualControllerAccessor GetPlayerAccesor(string idKey) {
        if (!PlayerInScene.ContainsKey(idKey))
            return null;
        return PlayerInScene[idKey]; }
    public static VirtualControllerAccessor GetPlayerAccesor(int indexIdKey) {
        if (!PlayerIndexInScene.ContainsKey(indexIdKey))
            return null;
        return PlayerIndexInScene[indexIdKey]; 
    }
    public static VirtualController GetPlayerController(string idKey) {
        VirtualControllerAccessor accessor = GetPlayerAccesor(idKey);
        return accessor == null ? null : accessor.Controller;
    }
    public static VirtualController GetPlayerController(int indexIdKey) { 
        VirtualControllerAccessor accessor =GetPlayerAccesor(indexIdKey); 
        return accessor==null?null:accessor.Controller; }
    void OnDestroy()
    {
        InstancesInScene.Remove(this);
        PlayerInScene.Remove(playerIdKey);
        PlayerIndexInScene.Remove(playerIndex);
    }


    void Update()
    {
        if (withDebug && Controller != null  )
        {
            description = Controller.GetActiveDataDescription();
                Debug.Log(description);
        }
    }
}
