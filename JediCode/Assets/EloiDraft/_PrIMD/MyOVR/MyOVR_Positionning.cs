﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(OVRCameraRig))]
public abstract class MyOVR_Positionning : MonoBehaviour
{

    private static MyOVR_Positionning  _lastInstanceActiveInScene;

    public static MyOVR_Positionning InstanceInScene
    {
        get { return _lastInstanceActiveInScene; }
       private set { _lastInstanceActiveInScene = value; }
    }




    private Vector3 _lastPosition;
    public Vector3 LastPosition
    {
        get { return _lastPosition; }
        private set { _lastPosition = value; }
    }
    private Quaternion _lastRotation;

    public Quaternion LastRotation
    {
        get { return _lastRotation; }
        private set { _lastRotation = value; }
    }
    private Vector3 _lastLocalPosition;

    public Vector3 LastLocalPosition
    {
        get { return _lastLocalPosition; }
        private set { _lastLocalPosition = value; }
    }
    private Quaternion _lastLocalRotation;

    public Quaternion LastLocalRotation
    {
        get { return _lastLocalRotation; }
        private set { _lastLocalRotation = value; }
    }
    

    
    private OVRCameraRig cameraRig;
    public Transform root;
    public void OnEnable()
    {
        if (cameraRig == null)
        {
            cameraRig = GetComponent<OVRCameraRig>() as OVRCameraRig;
        }
        if (cameraRig != null)
        {
            cameraRig.onPositionningUpdate += PositionOculusInScene;
        }

        //Singleton setter
        InstanceInScene = this;

    }

    public void OnDisable()
    {
        if (cameraRig != null)
        {
            cameraRig.onPositionningUpdate -= PositionOculusInScene;
        }


        //Singleton remover
        if (InstanceInScene == this)
            InstanceInScene = null;

    }


    protected abstract void PositionOculusInScene(OVRPose leftPose, OVRPose rightPose, Transform left, Transform center, Transform right,bool alreadyPositined);
    protected void ApplyPositionningAt(Transform leftOVREye, Transform OVRcenter, Transform rightOVREye, ref Vector3 centerPosition, ref Vector3 toGoToTheRight, ref Vector3 toGoToTheLeft, ref Quaternion orientationLeft, ref Quaternion orientationRight)
    {
        if (leftOVREye == null || OVRcenter == null || rightOVREye == null) return;

        leftOVREye.localPosition = centerPosition + toGoToTheLeft;
        OVRcenter.localPosition = centerPosition;
        rightOVREye.localPosition = centerPosition + toGoToTheRight;

        leftOVREye.localRotation = orientationLeft;
        OVRcenter.localRotation = orientationLeft;
        rightOVREye.localRotation = orientationRight;

        LastPosition = OVRcenter.position;
        LastRotation = OVRcenter.rotation;

        LastLocalPosition = OVRcenter.localPosition;
        LastLocalRotation = OVRcenter.localRotation;
    }
}