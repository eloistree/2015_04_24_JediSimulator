﻿using UnityEngine;
using System.Collections;

public class MyOVR_FakePositionTrackingV2 : MyOVR_Positionning
{

    public float backwardDistance = 0.1f;
    public float forwardDistance = 0.1f;
    public float leftRightDistance = 0.1f;
    public float heightDistance = 0.1f;
    public Vector3 offsetbaseAdjustment;
    public bool useIsLookingLeftRight = true;
    public bool useIsLookingUpDown = true;
    public bool useIsTited = true;

    public bool withLerp;
    public float lerpSpeed;


    protected override void PositionOculusInScene(OVRPose leftPose, OVRPose rightPose, Transform left, Transform center, Transform right, bool alreadyPositined)
    {
        if (left == null || center == null || right == null) return;
        if (root == null)
        {
            Debug.Log("No root define !", this.gameObject);
        }

        if (withLerp) {

            Debug.Log("Lerp is not implemented yet. In coming !");
            /* the idea is that some time, the behviour of the positionner switch heavily. 
             * So it would be important to lerp in aim to do not disturbe the player  when it happen.
             * But the lerp can be to slow or the player will notice it and break the immersion
             */
        }

        Vector3 centerPosition = (leftPose.position + rightPose.position) / 2f;
        Vector3 centerToRight = rightPose.position - centerPosition;
        Vector3 centerToLeft = leftPose.position - centerPosition;

        Vector3 localHeadPosition = Vector3.zero;
        bool isFirstAdd = true;
        if (useIsTited) { AddAverageValueIfNotNull(ref localHeadPosition, GetFrontalAdjustment(leftPose.orientation),isFirstAdd); isFirstAdd = false; }
        if (useIsLookingUpDown) { AddAverageValueIfNotNull(ref localHeadPosition, GetLateralAdjustment(leftPose.orientation), isFirstAdd); isFirstAdd = false; }
        if (useIsLookingLeftRight) { AddAverageValueIfNotNull(ref localHeadPosition, GetTopAdjustment(leftPose.orientation), isFirstAdd); isFirstAdd = false; }
      
        //Debug.Log(localHeadPosition);
        localHeadPosition.x *= leftRightDistance;
        localHeadPosition.z *= localHeadPosition.z<0f?backwardDistance : forwardDistance;
        localHeadPosition.y *= heightDistance;
        //centerPosition = root.position + offsetbaseAdjustment + localHeadPosition;
        centerPosition =  offsetbaseAdjustment + localHeadPosition;
        
        ApplyPositionningAt(left, center, right, ref centerPosition, ref centerToRight, ref centerToLeft, ref leftPose.orientation, ref rightPose.orientation);

    }

    private void AddAverageValueIfNotNull(ref Vector3 addAt, Vector3 value, bool isFirst=false)
    {
        if (isFirst) {
            addAt.x = value.x ;
            addAt.y = value.y;
            addAt.z = value.z ;
        }


        if (  value.x != 0f)
        {
            addAt.x = (addAt.x + value.x) / 2f;
        }
        if ( value.y != 0f)
        {
            addAt.y = (addAt.y + value.y) / 2f;
        }
        if ( value.z != 0f)
        {
            addAt.z = (addAt.z + value.z) / 2f;
        }
    }

    private float leftRightLimitBack = 4f;
    private float leftRightLimitFront = 1f;

    private Vector3 GetTopAdjustment(Quaternion headRotation)
    {
        Vector3 adjustment = Vector3.zero;
        Vector3 forwardOnXZ = headRotation * Vector3.forward;
        adjustment.x += forwardOnXZ.x;
        adjustment.z += forwardOnXZ.z < 0f ? forwardOnXZ.z / leftRightLimitBack : forwardOnXZ.z / leftRightLimitFront;
        return adjustment;
    }

    private Vector3 GetFrontalAdjustment(Quaternion headRotation)
    {
        Vector3 adjustment = Vector3.zero;

        Vector3 headFrontRotation = headRotation * Vector3.up;
        headFrontRotation.z = 0f;
        adjustment.y += headFrontRotation.y;
        adjustment.x += headFrontRotation.x;
        return adjustment;
    }
    private Vector3 GetLateralAdjustment(Quaternion headRotation)
    {
        Vector3 adjustment = Vector3.zero;
        Vector3 direction = headRotation * Vector3.forward;
        bool isLookingTop = direction.z < 0f && direction.y >= 0f;
        bool isLookingDown = direction.y <= -0f;
        bool isLookingFowardDown = direction.y <= 0f && (direction.y <= 0.8f && direction.y >= -0.8f);
      
 
        adjustment.y += direction.y;
        adjustment.z += direction.z;
        return adjustment;
        
    }


    public void IgnoreTopAxis(Quaternion rotation, out Quaternion newRotaion)
    {
        IgnoreAxis(rotation, out newRotaion, false, true);
    }
    public void IgnoreLeftAxis(Quaternion rotation, out Quaternion newRotaion)
    {

        IgnoreAxis(rotation, out newRotaion, true, false);
    }

    private void IgnoreAxis(Quaternion rotation, out Quaternion newRotation, bool ignoreX, bool ignoreY)
    {
        Vector3 newEuler = rotation.eulerAngles;

        if (ignoreX)
            newEuler.y = 0;
        if (ignoreY)
            newEuler.x = 0;
        newRotation = Quaternion.Euler(newEuler);

    }
}
