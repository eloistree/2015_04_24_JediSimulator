﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MobileAcceleroToVirutalController : MonoBehaviour {

    public VirtualController virutalController;
    public int joystickIndex;

    public Text debugDisplay;
    void Update()
    {
        if (virutalController != null)
            virutalController.SetJoystickValue(joystickIndex, HomidoUtility.GetAccelerationAsJoystick(0.5f, 0.5f, true, true));
        Debug.Log(">>  Accelerometer: " + Input.acceleration);
        debugDisplay.text = ">>  Accelerometer: " + Input.acceleration;
    }
}
