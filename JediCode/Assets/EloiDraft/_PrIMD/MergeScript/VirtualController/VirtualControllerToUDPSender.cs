﻿using UnityEngine;
using System.Collections;

public class VirtualControllerToUDPSender : MonoBehaviour
{
    public VirtualControllerAccessor accessor;
    public VirtualController VirtualControl{get{return accessor.Controller;}}
    public UDP_Sender udpSender;
    //public TCP_Sender tcpSender;
    public int playerIndex = 0;
    public string playerKeyId;
    public ListenToChange[] changeToSendByUdp;

    public bool withDebug;
    void Start () {
        Reset();

        foreach (ListenToChange changeListen in changeToSendByUdp) {
            SendUDPWhenChange(changeListen.whatToListen, changeListen.indexToListen);
        
        }
	}

    private void SendUDPWhenChange(ListenToChange.WhatToListenTo whatToListenTo, int index)
    {

        switch (whatToListenTo)
        {
            case ListenToChange.WhatToListenTo.Joystick:
                VirtualControl.GetJoystick(index).onValueChanged += JoystickChange;
                break;
            case ListenToChange.WhatToListenTo.Trigger:
                VirtualControl.GetTrigger(index).onValueChanged += TriggerChange;
                break;
            case ListenToChange.WhatToListenTo.Button:
                VirtualControl.GetButton(index).onValueChanged += ButtonChange;
                break;
            case ListenToChange.WhatToListenTo.Request:
                VirtualControl.GetRequests().onValueChanged += RequestChange;
                break;
            case ListenToChange.WhatToListenTo.Positioning:
                VirtualControl.GetPosition(index).onValueChanged += PositioningChange;
                break;
            case ListenToChange.WhatToListenTo.Orientation:
                VirtualControl.GetOrientation(index).onValueChanged += OrientationChange;
                break;
            case ListenToChange.WhatToListenTo.TouchPad:
                VirtualControl.GetTouchPad(index).onValueChanged += TouchPadChange;
                break;
            default:
                break;
        }

    }

    private void JoystickChange(int index, Vector3 oldValue, Vector3 newValue)
    {
        SendCommand(string.Format("Joy:{0}:{1}:{2}", index, newValue.x, newValue.y));
    }

    private void TriggerChange(int index, float oldValue, float newValue)
    {
        SendCommand(string.Format("Tri:{0}:{1}", index, newValue));
    }

    private void RequestChange(int index, string oldValue, string newValue)
    {
        SendCommand(string.Format("Req:{0}", newValue));
    }

    private void PositioningChange(int index, Vector3 oldValue, Vector3 newValue)
    {
        SendCommand(string.Format("Pos:{0}:{1}:{2}:{3}", index, newValue.x, newValue.y,newValue.z));
    }

    private void OrientationChange(int index, Quaternion oldValue, Quaternion newValue)
    {
        SendCommand(string.Format("Ori:{0}:{1}:{2}:{3}:{4}", index, newValue.x, newValue.y, newValue.z, newValue.w));
    }

    private void TouchPadChange(int index, Vector2 oldValue, Vector2 newValue)
    {

        SendCommand(string.Format("Pad:{0}:{1}:{2}", index, newValue.x, newValue.y));

    }

    private void ButtonChange(int index, bool oldValue, bool newValue)
    {
        SendCommand(string.Format("But:{0}:{1}",index,newValue?1:0));

    }
    private void SendCommand(string cmdToSend) { 
        if (!udpSender) return;
        string cmd = GetPlayerCmdStart();
        cmd += cmdToSend;
        if(withDebug)
        Debug.Log("Send... " + cmd);
        udpSender.Send(cmd);    
    }

    public string GetPlayerCmdStart() { 
        return string.Format("P:{0}:{1}|", playerIndex,playerKeyId);
    }

    void Reset() {
        string id =Network.player.ipAddress;
    if(id.Length>3) id = id.Substring(id.Length-3);
    id = id.Replace(".", "");
    playerKeyId = id;
    }


    [System.Serializable]
    public struct ListenToChange { 
        public enum WhatToListenTo {Joystick, Trigger, Button, Request, Positioning, Orientation, TouchPad}
        public WhatToListenTo whatToListen;
        public int indexToListen;
    }
}
