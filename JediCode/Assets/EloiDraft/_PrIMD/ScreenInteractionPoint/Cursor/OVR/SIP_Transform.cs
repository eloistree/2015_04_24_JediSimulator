﻿using UnityEngine;
using System.Collections;

public class SIP_Transform : MonoBehaviour, IScreenCursorInfoSource
{

    public Transform rootRef;
    public Camera screenCameraRef;
    public Vector3 ScreenCursorPosition;
    public LayerMask layerMaks;
    public float maxDistance= 10f;
    public bool CursorActive;
    public Transform debugCursorObj;


    public void Start() {
        Reset();
    }
    public void Reset() {
        ScreenCursorPosition = new Vector2(Screen.width / 2f, Screen.height / 2f);
        if (screenCameraRef == null) screenCameraRef = Camera.main;
    }

   
    public void Update() {
        CursorActive = IsOneCursorsActive() ;
        ScreenCursorPosition = GetScreenPosition(rootRef);
        
     }

    private Vector3 GetScreenPosition(Transform rootRef)
    {
        RaycastHit hit ;
        if (Physics.Raycast(rootRef.position, rootRef.forward, out hit, maxDistance, layerMaks))
        {
            Vector3 pos = hit.point;
            if (debugCursorObj != null)
            {
                debugCursorObj.position = pos;
                debugCursorObj.rotation = rootRef.rotation;
            }
            return screenCameraRef.WorldToScreenPoint(pos);


        }
        return screenCameraRef.ViewportToScreenPoint(new Vector3(0.5f, 0.5f, 0f));
    }

    public virtual bool IsOneCursorsActive() {
        return Input.GetMouseButton(0) || Input.touchCount > 0;
    }
   

    Vector3 IScreenCursorInfoSource.GetScreenPosition(ref Camera camera)
    {
        camera = screenCameraRef;
        return ScreenCursorPosition;
    }

    bool IScreenCursorInfoSource.IsCursorActive()
    {
        return CursorActive;
    }


    public bool GetCustumbuttonSelected(ref GameObject buttonSelected)
    {
        return false;
    }
}